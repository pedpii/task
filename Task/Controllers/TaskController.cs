﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RestSharp;
using Models;
using System.Web.Script.Serialization;
using System.Net;
using System.Reflection;
using PagedList;
using System.IO;
using Repo;
using System.Threading.Tasks;
using static Repo.sendEmail;

namespace Task.Controllers
{
    public class TaskController : Controller
    {
        JobsEntities db = new JobsEntities();
        private int pageSize = 10;
        //
        System.Globalization.CultureInfo en = new System.Globalization.CultureInfo("en-US");
        public ActionResult Index()
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            if (StaffLevel <= 0)
            {
                return RedirectToAction("LogIn", "Home");
            }
            if (StaffLevel >= 3)
            {
                return RedirectToAction("Index", "TsTask");
            }
            ViewBag.StaffLevel = StaffLevel;
            var aa = db.masStaffs.Where(w => w.Status > 0);

            if (aa.Count() > 0)
            {
                var rs = aa.ToList();
                List<dropdownAuto> dataDropdown = new List<dropdownAuto>();
                foreach (var s in rs)
                {
                    dataDropdown.Add(new dropdownAuto()
                    {
                        ID = s.ID,
                        Name = s.Name,
                        Email = s.Email ?? ""
                    });
                }
                ViewBag.StaffID = dataDropdown;
            }
            else
            {
                ViewBag.StaffID = new Models.dropdownAuto();
            }
            var bb = db.masTaskStatus.Where(w => w.Status > 0);
            if (bb.Count() > 0)
            {
                if (StaffLevel == 2)
                {
                    ViewBag.StatusID = bb.Where(w => w.ID == 1 || w.ID == 4 || w.ID == 2).Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList();
                }
                else if (StaffLevel == 1)
                {
                    ViewBag.StatusID = bb.Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList();
                }
            }
            else
            {
                ViewBag.StatusID = new Models.dropdownAuto();
            }
            var cc = db.masProjects.Where(w => w.Status > 0);
            if (cc.Count() > 0)
            {
                ViewBag.ProjectID = cc.Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList(); 
            }
            else
            {
                ViewBag.ProjectID = new Models.dropdownAuto(); 
            }

            ViewBag.TaskId = "-1";
            return View();
        }
        public ActionResult Details(int TaskId)
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            if (StaffLevel <= 0)
            {
                return RedirectToAction("LogIn", "Home");
            }
            ViewBag.StaffLevel = StaffLevel;
            var aa = db.masStaffs.Where(w => w.Status > 0);

            if (aa.Count() > 0)
            {
                var rs = aa.ToList();
                List<dropdownAuto> dataDropdown = new List<dropdownAuto>();
                foreach (var s in rs)
                {
                    dataDropdown.Add(new dropdownAuto()
                    {
                        ID = s.ID,
                        Name = s.Name,
                        Email = s.Email ?? ""
                    });
                }
                ViewBag.StaffID = dataDropdown;
            }
            else
            {
                ViewBag.StaffID = new Models.dropdownAuto();
            }
            var bb = db.masTaskStatus.Where(w => w.Status > 0);
            if (bb.Count() > 0)
            {
                //ViewBag.StatusID = new SelectList(bb.ToList(), "ID", "Name");
                if (StaffLevel == 2)
                {
                    ViewBag.StatusID = bb.Where(w => w.ID == 1 || w.ID == 4 || w.ID == 2).Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList();
                }
                else if (StaffLevel == 1)
                {
                    ViewBag.StatusID = bb.Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList();
                }
                //ViewBag.StatusID = bb.Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList();
            }
            else
            {
                ViewBag.StatusID = new Models.dropdownAuto();
            }
            var cc = db.masProjects.Where(w => w.Status > 0);
            if (cc.Count() > 0)
            {
                ViewBag.ProjectID = cc.Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList(); //ทำ ddl แบบบ Autocompleted
                //ViewBag.ProjectID = new SelectList(cc.ToList(), "ID", "Name"); //ทำddlเฉยๆ
            }
            else
            {
                ViewBag.ProjectID = new Models.dropdownAuto(); //ทำ ddl แบบบ Autocompleted
                //ViewBag.ProjectID = new SelectList("ID", "Name"); //ทำddlเฉยๆ
            }
            ViewBag.TaskId = TaskId;


            //ViewBag.StatusID = new SelectList(db.masTaskStatus.ToList(), "ID", "Name",-1);
            //ViewBag.ProjectID = new SelectList(db.masProjects.ToList(), "ID", "Name");
            return View("Index");
        }
        public ActionResult Detail(int TaskID)
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            var Tasks = EntityToModel(db.tranTasks.Find(TaskID));
            //List<ListFileName> FileName = new List<ListFileName>();
            Models.tranTasksh result = new tranTasksh();
            result.DueDate = Tasks.DueDate;
            result.ID = Tasks.ID;
            result.Name = Tasks.Name;
            result.ProjectID = Tasks.ProjectID;
            result.ProjectName = Tasks.ProjectName;
            result.Remark = Tasks.Remark;
            result.StaffID = Tasks.StaffID.ToString();
            result.StaffName = Tasks.StaffName;
            result.StatusName = Tasks.StatusName;
            result.PlatformID = Tasks.PlatformID;
            result.PlatformName = Tasks.PlatformName;
            result.FileID = Tasks.FileID;
            result.FileData = Tasks.FileData;
            result.FileNameTs = new List<ListFileNameTs>();
            result.FileName = new List<ListFileName>();
            List<string> typeImg = new List<string>();
            typeImg.Add(".jpg");
            typeImg.Add(".png");
            typeImg.Add(".jpeg");
            typeImg.Add(".gif");
            typeImg.Add(".svn");
            typeImg.Add(".pdf");

            string LocalpathFile = Properties.Settings.Default.LocalpathFile + "/Document/" + Tasks.ProjectID + "/" + Tasks.ID + "/";
            string LinkpathFile = Properties.Settings.Default.LinkURL + "/Document/" + Tasks.ProjectID + "/" + Tasks.ID + "/";
            if (Directory.Exists(LocalpathFile))
            {
                DirectoryInfo DirectoryFile = new DirectoryInfo(LocalpathFile);
                var Files = DirectoryFile.GetFiles();
                foreach (var ListFile in Files)
                {
                    bool TypeImgCheck = false;
                    var nameFile = ListFile.Name.Split('[', ']');
                    if (typeImg.Contains(ListFile.Extension))
                    {
                        TypeImgCheck = true;
                    }
                    result.FileName.Add(new ListFileName()
                    {
                        Name = nameFile[2],
                        FullName = ListFile.Name,
                        PathFile = LinkpathFile + ListFile.Name,
                        Review = TypeImgCheck
                    });
                }
                //================ Comment =================
                var getdata = db.tranTasks.Where(w => w.ID == TaskID).FirstOrDefault();
                if (getdata != null)
                {
                    var getcomment = getdata.mapTaskTsDetail.AsQueryable();
                    if (getcomment.Count() > 0)
                        LocalpathFile += "Comment";
                    foreach (var ListFile in getcomment)
                    {
                        if (Directory.Exists(LocalpathFile))
                        {
                            //File
                            DirectoryInfo DirectoryFile2 = new DirectoryInfo(LocalpathFile);
                            var OldFile = DirectoryFile2.GetFiles();
                            var OldFileName = OldFile.Where(w => w.Name.Contains(ListFile.ID.ToString())).FirstOrDefault();

                            bool TypeImgCheck = false;
                            if (typeImg.Contains(OldFileName.Extension))
                            {
                                TypeImgCheck = true;
                            }

                            //Comemet & IDmapTask
                            if (ListFile.ID > 0)
                            {
                                result.FileNameTs.Add(new ListFileNameTs()
                                {
                                    IDmapTask = ListFile.ID,
                                    Comment = ListFile.Comment == null ? "" : ListFile.Comment,
                                    OldFiles = OldFileName.Name == null ? " " : OldFileName.Name.ToString(),
                                    PathFiles = LinkpathFile + "Comment/" + OldFileName.Name,
                                    ReviewFiles = TypeImgCheck
                                });
                            }
                        }
                    }
                }
            }

            //result.FileName = FileName;

            return PartialView(result);
        }
        public ActionResult List(Models.Task_Filter Filter, int pageNO = 1)
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            ViewBag.StaffLevel = StaffLevel;
            string IDStaff = "";
            int IDStaff_num = 0;
            if (HttpContext.Session["LoginID"] != null)
            {
                IDStaff = HttpContext.Session["IDStaff"].ToString();

                IDStaff_num = int.Parse(IDStaff);
                ViewBag.IDStaff = IDStaff_num;
            }
            else
            {
                return RedirectToAction("LogIn", "Home");
            }

            try
            {
                var Task = db.tranTasks.AsQueryable();
                if (StaffLevel != 1)
                {
                    Task = Task.Where(s => s.StaffID == IDStaff_num);
                }
                if (Filter != null)
                {
                    if (Filter.UnShowFinish)
                    {
                        Task = Task.Where(w => w.StatusID != 2);
                    }
                    if (Filter.UnShowCancel)
                    {
                        Task = Task.Where(w => w.StatusID != 0);
                    }
                    if (Filter.StaffID > 0 || !String.IsNullOrEmpty(Filter.StaffName))
                    {
                        if (!String.IsNullOrEmpty(Filter.StaffName))
                        {
                            Task = Task.Where(w => w.masStaffs.Name.Contains(Filter.StaffName)).AsQueryable();
                        }
                        else
                        {
                            Task = Task.Where(w => w.StaffID == Filter.StaffID).AsQueryable();
                        }
                        //Task = Task.Where(w => w.StaffID == Filter.StaffID).AsQueryable();
                    }
                    if (Filter.StatusID > 0 || !String.IsNullOrEmpty(Filter.StatusName))
                    {
                        if (!String.IsNullOrEmpty(Filter.StatusName))
                        {
                            Task = Task.Where(w => w.masTaskStatus.Name.Contains(Filter.StatusName)).AsQueryable();
                        }
                        else
                        {
                            Task = Task.Where(w => w.StatusID == Filter.StatusID).AsQueryable();
                        }
                    }
                    if (Filter.StatusID > 0 || !String.IsNullOrEmpty(Filter.PlatformName))
                    {
                        if (!String.IsNullOrEmpty(Filter.PlatformName))
                        {
                            Task = Task.Where(w => w.masTaskStatus.Name.Contains(Filter.PlatformName)).AsQueryable();
                        }
                        else
                        {
                            Task = Task.Where(w => w.StatusID == Filter.StatusID).AsQueryable();
                        }
                    }
                    if (Filter.ProjectID > 0 || !String.IsNullOrEmpty(Filter.ProjectName))
                    {
                        if (!String.IsNullOrEmpty(Filter.ProjectName))
                        {
                            Task = Task.Where(w => w.masProjects.Name.Contains(Filter.ProjectName)).AsQueryable();
                        }
                        else
                        {
                            Task = Task.Where(w => w.ProjectID == Filter.ProjectID).AsQueryable();
                        }
                        //Task = Task.Where(w => w.ProjectID == Filter.ProjectID).AsQueryable();
                    }
                }



                if (Filter.FilterDueDate != null)
                {
                    //DateTime DueDateBegin = Filter.FilterDueDate.Value.Date;
                    DateTime DueDateEnd = Filter.FilterDueDate.Value.AddDays(1).Date.AddMinutes(-1);
                    Task = Task.Where(w => w.DueDate <= DueDateEnd).AsQueryable();
                }

                List<Models.tranTask> MTask = new List<Models.tranTask>();
                MTask = Task.ToList().Select(EntityToModel).ToList();
                int.TryParse(Properties.Settings.Default.pageSize.ToString(), out pageSize);
                if (pageSize <= 0) { pageSize = 10; }
                int pageIdx = pageNO - 1;
                int totalItemCount = MTask.Count();
                var paged = MTask.OrderBy(o => o.DueDate).Skip(pageIdx * pageSize).Take(pageSize);
                var pageresult = new StaticPagedList<Models.tranTask>(paged, pageNO, pageSize, totalItemCount);
                return PartialView(pageresult);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
        public ActionResult ListNew(Models.Task_Filter Filter, int pageNO2 = 1)
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            if (StaffLevel <= 0)
            {
                return RedirectToAction("LogIn", "Home");
            }
            ViewBag.StaffLevel = StaffLevel;
            string IDStaff = "";
            int IDStaff_num = 0;
            if (HttpContext.Session["LoginID"] != null)
            {
                IDStaff = HttpContext.Session["IDStaff"].ToString();

                IDStaff_num = int.Parse(IDStaff);
                ViewBag.IDStaff = IDStaff_num;
            }
            else
            {
                return RedirectToAction("LogIn", "Home");
            }
            try
            {
                var Task = db.tranTasks.AsQueryable();
                //if (StaffLevel != 1)
                //{
                //    Task = Task.Where(s => s.StaffID == IDStaff_num);
                //}
                //if (StaffLevel == 1)
                //    Task =   Task.Where(w => w.StatusID == 1 || w.StatusID == 4).AsQueryable();
                //else
                Task = Task.Where(w => w.StatusID == 3).AsQueryable();
                List<Models.tranTask> MTask = new List<Models.tranTask>();
                MTask = Task.Select(EntityToModel).ToList();
                int.TryParse(Properties.Settings.Default.pageSize.ToString(), out pageSize);
                if (pageSize <= 0) { pageSize = 10; }
                int pageIdx = pageNO2 - 1;
                int totalItemCount = MTask.Count();
                var paged = MTask.OrderBy(o => o.DueDate).Skip(pageIdx * pageSize).Take(pageSize);
                var pageresult = new StaticPagedList<Models.tranTask>(paged, pageNO2, pageSize, totalItemCount);
                return PartialView(pageresult);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
        private Models.tranTask EntityToModel(tranTasks Value)
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];

            Models.tranTask data = new Models.tranTask();
            try
            {

                data.ID = Value.ID;
                data.Name = Value.Name;
                data.DueDate = Value.DueDate == null ? DateTime.Now.ToString("dd/MMM/yyyy", en) : ((DateTime)Value.DueDate).ToString("dd/MMM/yyyy", en);
                data.ProjectID = Value.ProjectID;
                data.ProjectName = Value.masProjects == null ? " " : Value.masProjects.Name;
                data.Remark = Value.Remark;
                data.StaffID = Value.StaffID == null ? -1 : Value.StaffID;
                data.StaffName = Value.masStaffs == null ? "" : Value.masStaffs.Name;
                data.StartDate = Value.StartDate == null ? DateTime.Now.ToString("dd/MMM/yyyy", en) : ((DateTime)Value.StartDate).ToString("dd/MMM/yyyy", en);
                data.StatusID = Value.StatusID;
                data.StatusName = Value.masTaskStatus == null ? "" : Value.masTaskStatus.Name;
                data.PlatformID = Value.PlatformID == null ? -1 : (int)Value.PlatformID;
                data.PlatformName = Value.masPlatform.Name;
                data.RecevierID = Value.tranHistory.Select(s => s.masStaffs1.ID).FirstOrDefault();
                data.RecevierIDName = Value.tranHistory.Select(s => s.masStaffs1.Name).FirstOrDefault();
                data.SenderID = Value.tranHistory.Select(s => s.masStaffs.ID).FirstOrDefault();
                data.SenderName = Value.tranHistory.Select(s => s.masStaffs.Name).FirstOrDefault();
                //data.StaffLevel = Value.masStaffs.StaffLevel;
                //FileData = Value.FileData,
                //FileID = Value.FileID,
                //FileName = Value.FileName,
                data.Approve = Value.Approve;
                //Comment = Value.Comment
            }
            catch (Exception ex)
            {

            }

            return data;

            //return new Models.tranTask()
            //{
            //ID = Value.ID,
            //Name = Value.Name,
            //DueDate = Value.DueDate == null ? DateTime.Now.ToString("dd/MMM/yyyy", en) : ((DateTime)Value.DueDate).ToString("dd/MMM/yyyy", en),
            //ProjectID = Value.ProjectID,
            //ProjectName = Value.masProjects==null ? " ": Value.masProjects.Name,
            //Remark = Value.Remark,
            //StaffID = Value.StaffID == null ? -1 : Value.StaffID,
            //StaffName = Value.masStaffs == null ? "" : Value.masStaffs.Name,
            //StartDate = Value.StartDate == null ? DateTime.Now.ToString("dd/MMM/yyyy", en) : ((DateTime)Value.StartDate).ToString("dd/MMM/yyyy", en),
            //StatusID = Value.StatusID,
            //StatusName = Value.masTaskStatus==null ?"": Value.masTaskStatus.Name,
            //PlatformID = Value.PlatformID == null ? -1 : (int)Value.PlatformID,
            //PlatformName = Value.masPlatform.Name,
            //RecevierID = Value.tranHistory.Select(s => s.masStaffs.ID).FirstOrDefault(),
            //RecevierIDName = Value.tranHistory.Select(s => s.masStaffs.Name).FirstOrDefault(),
            //SenderID = Value.tranHistory.Select(s => s.masStaffs.ID).FirstOrDefault(),
            //SenderName = Value.tranHistory.Select(s => s.masStaffs.Name).FirstOrDefault(),
            //StaffLevel = Value.masStaffs.StaffLevel,
            ////FileData = Value.FileData,
            ////FileID = Value.FileID,
            ////FileName = Value.FileName,
            //Approve = Value.Approve,
            ////Comment = Value.Comment


            //};
        }
        public ActionResult Create()
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            if (StaffLevel <= 0)
            {
                return RedirectToAction("LogIn", "Home");
            }
            ViewBag.StaffLevel = StaffLevel;
            var getidmasProjects = db.masProjects.Where(w => w.Status > 0);
            if (getidmasProjects.Count() > 0)
            {
                ViewBag.ProjectID = getidmasProjects.Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList();
            }
            else
            {
                ViewBag.ProjectID = new Models.dropdownAuto();
            }
            //var getmasProjects = db.masProjects.Where(w => w.Status > 0); // ddl ธรรมดา
            //if (getmasProjects.Count() > 0)
            //{
            //    ViewBag.ProjectID = new SelectList(getmasProjects.ToList(), "ID", "Name");
            //}
            //else
            //{
            //    ViewBag.ProjectID = new SelectList("ID", "Name");
            //}
            var getidmasStaffs = db.masStaffs.Where(w => w.Status > 0);
            if (getidmasStaffs.Count() > 0)
            {
                var rs = getidmasStaffs.ToList();
                List<dropdownAuto> dataDropdown = new List<dropdownAuto>();
                foreach (var s in rs)
                {
                    dataDropdown.Add(new dropdownAuto()
                    {
                        ID = s.ID,
                        Name = s.Name,
                        Email = s.Email ?? ""
                    });
                }
                ViewBag.StaffID = dataDropdown;
            }
            else
            {
                ViewBag.StaffID = new Models.dropdownAuto();
            }

            var getmasStaffsTS = db.masStaffs.Where(w => w.StaffLevel == 3);
            if (getmasStaffsTS.Count() > 0)
            {
                var rs = getmasStaffsTS.ToList();
                List<dropdownAuto> dataDropdownTS = new List<dropdownAuto>();
                foreach (var s in rs)
                {
                    dataDropdownTS.Add(new dropdownAuto()
                    {
                        ID = s.ID,
                        Name = s.Name,
                        Email = s.Email ?? ""
                    });
                }
                ViewBag.StaffIDTS = dataDropdownTS;

            }
            else
            {
                ViewBag.StaffIDTS = new Models.dropdownAuto();
            }


            var getidmasTaskStatus = db.masTaskStatus.Where(w => w.Status > 0);
            if (getidmasTaskStatus.Count() > 0)
            {
                if (StaffLevel == 2)
                {
                    ViewBag.StatusID = getidmasTaskStatus.Where(w => w.ID == 1 || w.ID == 4 || w.ID == 2).Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList();
                }
                else if (StaffLevel == 1)
                {
                    ViewBag.StatusID = getidmasTaskStatus.Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList();
                }
                //ViewBag.StatusID = getidmasTaskStatus.Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList();
            }
            else
            {
                ViewBag.StatusID = new Models.dropdownAuto();
            }
            //var getmasTaskStatus = db.masTaskStatus.Where(w => w.Status > 0);
            //if (getmasTaskStatus.Count() > 0)
            //{
            //    ViewBag.StatusID = new SelectList(getmasTaskStatus.ToList(), "ID", "Name");
            //}
            //else
            //{
            //    ViewBag.StatusID = new SelectList("ID", "Name");
            //}
            //ViewBag.ProjectID = new SelectList(db.masProjects, "ID", "Name");
            //ViewBag.StaffID = new SelectList(db.masStaffs, "ID", "Name");
            //var a = db.masTaskStatus.Where(w => w.ID != 3).ToList();
            //a.Insert(0, db.masTaskStatus.FirstOrDefault(w => w.ID == 3));
            //ViewBag.StatusID = new SelectList(a, "ID", "Name");
            var dd = db.masPlatform.Where(w => w.Status > 0).ToList();
            if (dd.Count() > 0)
            {
                ViewBag.PlatformID = dd.Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList();
            }
            else
            {
                ViewBag.PlatformID = new Models.dropdownAuto();
            }
            tranTask tasks = new tranTask();
            tasks.StartDate = DateTime.Now.ToString("dd/MMM/yyyy", en);
            tasks.DueDate = DateTime.Now.ToString("dd/MMM/yyyy", en);
            return PartialView(tasks);
        }

        // POST: /SettingTasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Models.tranTask Item)
        {

            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            string ErrorMSG = ValidForNew(Item, true); //ฟังชั่นเช็คค่าว่างกับตัวอักษร true กับ false
            if (!ErrorMSG.Equals(string.Empty))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ErrorMSG);
            }
            var getcount = db.tranTasks;
            DateTime Start = new DateTime();
            DateTime.TryParse(Item.StartDate, out Start);//edit
            DateTime Deadline = new DateTime();
            DateTime.TryParse(Item.DueDate, out Deadline);
            tranTasks tranTsk = new tranTasks()
            {
                ProjectID = Item.ProjectID,
                Name = Item.Name == null ? "" : Item.Name,
                Remark = Item.Remark == null ? "" : Item.Remark,
                DueDate = Deadline,
                StartDate = Start,
                StaffID = Item.StaffID,
                StatusID = Item.StatusID,
                PlatformID = Item.PlatformID
            };

            db.tranTasks.Add(tranTsk);
            try
            {
                db.SaveChanges();

                var Tasks = getcount.OrderByDescending(w => w.ID).FirstOrDefault();
                int id = 0;
                if (Tasks != null)
                {
                    id = Tasks.ID;
                }

                string path = Properties.Settings.Default.LocalpathFile + "\\Document\\" + Item.ProjectID + "\\" + id + "\\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                if (Item.FileData != null)
                {
                    if (Item.FileData.Count > 0)
                    {
                        foreach (var data in Item.FileData)
                        {
                            Guid genRand = Guid.NewGuid();
                            data.Files.SaveAs(path + "[" + genRand.ToString() + "]" + data.Files.FileName);
                        }
                    }
                }

                string urlte = Task.Properties.Settings.Default.LinkURL + "Task/Details?TaskId=" + tranTsk.ID.ToString();
                string LineToken = Properties.Settings.Default.LineToken;
                string LineApi = Properties.Settings.Default.LineApi;
                //string urltest2 = "Details?TaskId=" + idTask.ToString();
                var msg = "มีการสร้าง Task " + "\n" + "ProjectID : " + Item.ProjectName + "\n" + "Task : " + Item.Name + "\n" + "วันที่เริ่มงาน : "
                    + Item.StartDate + "\n" + "กำหนดส่ง : " + Item.DueDate + "\n" + "Remark : " + Item.Remark + "\n" + "ผู้รับผิดชอบ : "
                    + Item.StaffName + "\n" + "สถานะ : " + Item.StatusName + "\n" + "รูปแบบ : " + Item.PlatformName + "\n"
                    + "Link : " + urlte;

                //LineNoti.lineNotify(msg, LineToken, LineApi);
                SendLine(msg, LineToken, LineApi);

                Item.imsg = 1;

                Models.SendEmail send = new Models.SendEmail();
                send.ID = Item.ID;
                send.Name = Item.Name;
                send.ProjectName = Item.ProjectName;
                send.StaffName = Item.StaffName;
                send.DueDate = Item.DueDate;
                send.Remark = Item.Remark;
                send.StatusName = Item.StatusName;
                send.PlatformName = Item.PlatformName;

                SendEmail(send, Item.ID);
            }
            catch (Exception ex)
            {
            }
            
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        //public ActionResult Create([Bind(Include = "ID,ProjectID,Name,Remark,DueDate,StartDate,StaffID,StatusID")] tranTask trantask)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            if (trantask.StaffID <= 0)
        //                trantask.StaffID = null;
        //            db.tranTasks.Add(trantask);
        //            db.SaveChanges();
        //        }
        //        catch (Exception ex)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        }
        //    }

        //    return new HttpStatusCodeResult(HttpStatusCode.OK);
        //}

        public ActionResult Edit(int TaskID)
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            if (StaffLevel <= 0)
            {
                return RedirectToAction("LogIn", "Home");
            }
            ViewBag.StaffLevel = StaffLevel;
            Models.tranTasksh result = new tranTasksh();
            var Task = db.tranTasks.Find(TaskID);
            var getmasProjects = db.masProjects.Where(w => w.Status > 0);
            if (getmasProjects.Count() > 0)
            {
                ViewBag.ProjectID = getmasProjects.Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList();
            }
            else
            {
                ViewBag.ProjectID = new Models.dropdownAuto();
            }
            var getmasStaffs = db.masStaffs.Where(w => w.Status > 0);
            if (getmasStaffs.Count() > 0)
            {
                var rs = getmasStaffs.ToList();
                List<dropdownAuto> dataDropdown = new List<dropdownAuto>();
                foreach (var s in rs)
                {
                    dataDropdown.Add(new dropdownAuto()
                    {
                        ID = s.ID,
                        Name = s.Name,
                        Email = s.Email ?? ""
                    });
                }
                ViewBag.StaffID = dataDropdown;

            }
            else
            {
                ViewBag.StaffID = new Models.dropdownAuto();
            }

            var getmasStaffsTS = db.masStaffs.Where(w => w.StaffLevel == 3);
            if (getmasStaffsTS.Count() > 0)
            {
                var rs = getmasStaffsTS.ToList();
                List<dropdownAuto> dataDropdownTS = new List<dropdownAuto>();
                foreach (var s in rs)
                {
                    dataDropdownTS.Add(new dropdownAuto()
                    {
                        ID = s.ID,
                        Name = s.Name,
                        Email = s.Email ?? ""
                    });
                }
                ViewBag.StaffIDTS = dataDropdownTS;

            }
            else
            {
                ViewBag.StaffIDTS = new Models.dropdownAuto();
            }

            var getmasTaskStatus = db.masTaskStatus.Where(w => w.Status > 0);
            if (getmasTaskStatus.Count() > 0)
            {
                if (StaffLevel == 2)
                {
                    ViewBag.StatusID = getmasTaskStatus.Where(w => w.ID == 1 || w.ID == 4 || w.ID == 2).Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList();
                }
                else if (StaffLevel == 1)
                {
                    ViewBag.StatusID = getmasTaskStatus.Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList();
                }

            }
            else
            {
                ViewBag.StatusID = new Models.dropdownAuto();
            }
            var dd = db.masPlatform.Where(w => w.Status > 0);
            if (dd.Count() > 0)
            {
                ViewBag.PlatformID = dd.Select(s => new Models.dropdownAuto { ID = s.ID, Name = s.Name }).ToList();
            }
            else
            {
                ViewBag.PlatformID = new Models.dropdownAuto();
            }

            List<ListFileName> FileName = new List<ListFileName>();
            string LinkpathFile = Properties.Settings.Default.LocalpathFile + "\\Document\\" + Task.ProjectID + "\\" + Task.ID + "\\";
            if (Directory.Exists(LinkpathFile))
            {
                DirectoryInfo DirectoryFile = new DirectoryInfo(LinkpathFile);
                var Files = DirectoryFile.GetFiles();

                foreach (var ListFile in Files)
                {
                    var nameFile = ListFile.Name.Split('[', ']');
                    Guid IDGuid = new Guid();
                    FileName.Add(new ListFileName()
                    {
                        Name = nameFile[2],
                        FullName = ListFile.Name,
                        PathFile = Properties.Settings.Default.LocalpathFile + "\\Document\\",
                        GUID = nameFile[1],
                        //FilesS.InputStream.CopyTo
                    });
                }
            }
            ViewBag.Files = FileName;
            result.FileName = new List<ListFileName>();
            return PartialView(EntityToModel(Task));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Models.tranTask Item)
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            string ErrorMSG = ValidForNew(Item, false); //ฟังชั่นเช็คค่าว่างกับตัวอักษร true กับ false
            if (!ErrorMSG.Equals(string.Empty))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ErrorMSG);
            }
            if (Item != null)
            {
                var getidtranTasks = db.tranTasks.FirstOrDefault(w => w.ID == Item.ID);
                if (getidtranTasks != null)
                {
                    getidtranTasks.StatusID = Item.StatusID;
                    getidtranTasks.Approve = 0;
                }
                if (Item.StatusID == 4)
                {
                    var gettranHis = db.tranHistory.FirstOrDefault(w => w.TraskID == Item.ID && w.Sender == Item.StaffID && w.Receiver == Item.RecevierID);
                    tranHistory tranHis = new tranHistory()
                    {
                        TraskID = Item.ID,
                        Receiver = Item.RecevierID,
                        Sender = Item.StaffID.GetValueOrDefault(),
                        DateRecode = DateTime.Now,
                    };
                    db.tranHistory.Add(tranHis);
                }
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }

            }


            #region folder path
            List<ListFileName> FileName = new List<ListFileName>();
            string LocalpathFile = Properties.Settings.Default.LocalpathFile + "Document\\" + Item.ProjectID + "\\" + Item.ID + "\\";
            if (!Directory.Exists(LocalpathFile))
            {
                Directory.CreateDirectory(LocalpathFile);
            }
            if (Item.FileData != null)
            {
                if (Directory.Exists(LocalpathFile))
                {
                    DirectoryInfo DirectoryFile = new DirectoryInfo(LocalpathFile);
                    var ltFiles = DirectoryFile.GetFiles();

                    //Delete File
                    foreach (var ListFile in ltFiles)
                    {

                        var CheckFile = Item.FileData.Where(w => w.OldName == ListFile.Name).Any();
                        if (!CheckFile)
                        {
                            if (System.IO.File.Exists(ListFile.FullName))
                            {
                                System.IO.File.Delete(ListFile.FullName);
                            }
                        }
                    }

                    //Add Files
                    var ltNewFiles = Item.FileData.Where(w => w.Files != null).Select(s => s.Files).ToList();
                    foreach (var dataFile in ltNewFiles)
                    {
                        Guid genRand = Guid.NewGuid();
                        dataFile.SaveAs(LocalpathFile + "[" + genRand.ToString() + "]" + dataFile.FileName);
                    }

                }

            }
            else
            {
                if (Directory.Exists(LocalpathFile))
                {
                    DirectoryInfo DirectoryFile = new DirectoryInfo(LocalpathFile);
                    var ltFiles = DirectoryFile.GetFiles();
                    foreach (var ListFile in ltFiles)
                    {
                        System.IO.File.Delete(ListFile.FullName);
                    }
                }
            }
            #endregion


            //string urltest2 = "Details?TaskId=" + idTask.ToString();

            string LineToken = Properties.Settings.Default.LineToken;
            string LineApi = Properties.Settings.Default.LineApi;
            var msg = "";
            var urlte = "";
            if (Item.StatusID == 4)
            {
                urlte = Task.Properties.Settings.Default.LinkURL + "TsTask/TsEdits?TaskId=" + Item.ID.ToString();
            }
            else
            {
                urlte = Task.Properties.Settings.Default.LinkURL + "Task/TsEdits?TaskId=" + Item.ID.ToString();
            }
            if (Item.StatusName == "")
            {

                msg = "มีการแก้ไข Task " + "\n" + "ProjectID : " + Item.ProjectName + "\n" + "Task : " + Item.Name + "\n" + "วันที่เริ่มงาน : "
                    + Item.StartDate + "\n" + "กำหนดส่ง : " + Item.DueDate + "\n" + "Remark : " + Item.Remark + "\n" + "ผู้รับผิดชอบ : "
                    + Item.StaffName + "\n" + "สถานะ : " + Item.StatusName + "\n" + "รูปแบบ : " + Item.PlatformName + "\n"
                    + "Link : " + urlte;
            }
            else
            {

                msg = "มีการแก้ไข Task " + "\n" + "ProjectID : " + Item.ProjectName + "\n" + "Task : " + Item.Name + "\n" + "วันที่เริ่มงาน : "
                    + Item.StartDate + "\n" + "กำหนดส่ง : " + Item.DueDate + "\n" + "Remark : " + Item.Remark + "\n" + "ผู้รับผิดชอบ : "
                    + Item.StaffName + "\n" + "สถานะ : " + Item.StatusName + "\n" + "รูปแบบ : " + Item.PlatformName + "\n"
                    + "Link : " + urlte;
            }

            SendLine(msg, LineToken, LineApi);
            Item.imsg = 2;

            Models.SendEmail send = new Models.SendEmail();
            send.ID = Item.ID;
            send.Name = Item.Name;
            send.ProjectName = Item.ProjectName;
            send.StaffName = Item.StaffName;
            send.DueDate = Item.DueDate;
            send.Remark = Item.Remark;
            send.StatusName = Item.StatusName;
            send.PlatformName = Item.PlatformName;

            SendEmail(send, Item.ID);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        public string ValidForNew(Models.tranTask Item, bool create = true) //create=true ให้เช็คค่าเลยกำหนดใน create กับ Edit ที่สร้างมา
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            string resume = string.Empty;
            string ErrorAlert = string.Empty;
            string ErrorRow = string.Empty;
            var getnameProject = db.masProjects.FirstOrDefault(s => s.Name == Item.ProjectName);
            var getnameStaff = db.masStaffs.FirstOrDefault(s => s.Name == Item.StaffName);
            var getnameStatus = db.masTaskStatus.FirstOrDefault(s => s.Name == Item.StatusName);
            var dd = db.masTaskStatus.FirstOrDefault(s => s.Name == Item.PlatformName);
            var getPlatform = db.masPlatform.FirstOrDefault(s => s.Name == Item.PlatformName);
            DateTime dDate;


            //int n;

            if (Item != null)
            {
                if (create)
                {
                    if (Item.ProjectID == 0)
                    {
                        resume += "BA,";
                    }
                    if (getnameProject == null)
                    {
                        resume += "BB,";
                    }
                    //if(Item.StartDate == null)
                    //{
                    //    resume += "IA,";
                    //}
                    //if (Item.DueDate == null)
                    //{
                    //    resume += "JA,";
                    //}
                    if (Item.Name != null)
                    {

                        if (Item.Name.Count() >= 255)
                        {
                            resume += "AB,";
                        }
                    }
                    else
                    {
                        resume += "AA,";
                    }

                    if (Item.StatusID == 0)
                    {
                        resume += "DA,";
                    }
                    if (getnameStatus == null)
                    {
                        resume += "DB,";
                    }

                    if (Item.FileData != null)
                    {
                        if (Item.FileData.Count >= 0)
                        {
                            foreach (var value in Item.FileData)
                            {
                                if (String.IsNullOrEmpty(value.OldName))
                                {
                                    if (value.Files == null)
                                    {
                                        ErrorRow += "GA,";
                                    }

                                }
                                ErrorRow += ";";
                            }
                        }

                    }

                    if (Item.StaffName != null)
                    {
                        if (getnameStaff == null)
                        {
                            resume += "CA,";
                        }
                    }
                    if (Item.PlatformID == 0)
                    {
                        resume += "HA,";
                    }
                    if (getPlatform == null)
                    {
                        resume += "HB,";
                    }

                    if (!String.IsNullOrEmpty(Item.DueDate))
                    {
                        if (!DateTime.TryParse(Item.DueDate, out dDate))
                        {
                            resume += "NA,";
                        }
                    }

                    if (!String.IsNullOrEmpty(Item.StartDate))
                    {
                        if (!DateTime.TryParse(Item.StartDate, out dDate))
                        {
                            resume += "MA,";
                        }
                    }
                }
                else
                {

                    if (Item.ProjectID == 0)
                    {
                        resume += "BA,";
                    }
                    if (getnameProject == null)
                    {
                        resume += "BB,";
                    }
                    if (Item.Name == null)
                    {
                        resume += "AA,";
                    }
                    else
                    {
                        if (Item.Name.Count() >= 255)
                        {
                            resume += "AB,";
                        }
                    }

                    if (Item.StatusID == 0)
                    {
                        resume += "DA,";
                    }
                    if (getnameStatus == null)
                    {
                        resume += "DB,";
                    }
                    if (Item.FileData != null)
                    {
                        if (Item.FileData.Count >= 0)
                        {
                            foreach (var value in Item.FileData)
                            {
                                if (String.IsNullOrEmpty(value.OldName))
                                {
                                    if (value.Files == null)
                                    {
                                        ErrorRow += "GA,";
                                    }

                                }
                                ErrorRow += ";";

                            }
                        }
                    }

                    if (Item.StaffName != null)
                    {
                        if (getnameStaff == null)
                        {
                            resume += "CA,";
                        }
                    }
                    if (Item.PlatformID == 0)
                    {
                        resume += "HA,";
                    }
                    if (getPlatform == null)
                    {
                        resume += "HB,";
                    }

                    if (!String.IsNullOrEmpty(Item.DueDate))
                    {
                        if (!DateTime.TryParse(Item.DueDate, out dDate))
                        {
                            resume += "NA,";
                        }
                    }

                    if (!String.IsNullOrEmpty(Item.StartDate))
                    {
                        if (!DateTime.TryParse(Item.StartDate, out dDate))
                        {
                            resume += "MA,";
                        }
                    }

                    if (Item.RecevierID == -1)
                    {
                        resume += "KA,";
                    }
                }


            }

            resume += "/" + ErrorRow;
            resume += "/" + ErrorAlert;
            string resumetest = resume.Replace('/', ' ');
            resumetest = resumetest.Replace(';', ' ');
            if (resumetest.Trim() == string.Empty) { resume = string.Empty; }
            return resume;
        }

        public PartialViewResult AddRowDetail()
        {
            Guid genRand = Guid.NewGuid();
            ViewBag.HtmlFieldPrefix = "FileName[" + genRand.ToString() + "]";
            ViewBag.Index = genRand.ToString();

            Models.FileName File = new FileName();
            return PartialView("~/Views/task/DetailRow.cshtml", File);
        }

        static async void SendEmail(Models.SendEmail Item, int ID)
        {
            await System.Threading.Tasks.Task.Run(() => Repo.sendEmail.SendMailToUser(Item, Item.ID));
        }
        static async void SendLine(string msg, string LineToken, string LineApi)
        {
            await System.Threading.Tasks.Task.Run(() => LineNoti.lineNotify(msg, LineToken, LineApi));
        }
    }
}
