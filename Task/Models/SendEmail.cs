﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Models
{
    public partial class SendEmail
    {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
        public string DueDate { get; set; }
        public string StartDate { get; set; }
        public Nullable<int> StaffID { get; set; }
        public string StaffName { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }
        public string PlatformName { get; set; }
        public int imsg { get; set; }
        

    }
}