
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/09/2019 14:28:23
-- Generated from EDMX file: C:\Users\Jaojuan\Documents\Visual Studio 2015\Projects\Task\Task\TaskDB.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [jobs01];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_tranTask_masProject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tranTasks] DROP CONSTRAINT [FK_tranTask_masProject];
GO
IF OBJECT_ID(N'[dbo].[FK_tranTask_masStaff]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tranTasks] DROP CONSTRAINT [FK_tranTask_masStaff];
GO
IF OBJECT_ID(N'[dbo].[FK_tranTask_masTaskStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tranTasks] DROP CONSTRAINT [FK_tranTask_masTaskStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_tranTasks_masPlatform]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tranTasks] DROP CONSTRAINT [FK_tranTasks_masPlatform];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[masPlatform]', 'U') IS NOT NULL
    DROP TABLE [dbo].[masPlatform];
GO
IF OBJECT_ID(N'[dbo].[masProjects]', 'U') IS NOT NULL
    DROP TABLE [dbo].[masProjects];
GO
IF OBJECT_ID(N'[dbo].[masStaffs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[masStaffs];
GO
IF OBJECT_ID(N'[dbo].[masTaskStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[masTaskStatus];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[tranTasks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tranTasks];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'masProjects'
CREATE TABLE [dbo].[masProjects] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(150)  NOT NULL,
    [Recorder] nvarchar(150)  NOT NULL,
    [RecordDate] datetime  NOT NULL,
    [Status] int  NOT NULL
);
GO

-- Creating table 'masStaffs'
CREATE TABLE [dbo].[masStaffs] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(150)  NOT NULL,
    [Recorder] nvarchar(150)  NOT NULL,
    [RecordDate] datetime  NOT NULL,
    [UserLogin] varchar(50)  NULL,
    [PasswordLogin] varchar(50)  NULL,
    [StaffLevel] int  NOT NULL,
    [Status] int  NOT NULL
);
GO

-- Creating table 'masTaskStatus'
CREATE TABLE [dbo].[masTaskStatus] (
    [ID] int  NOT NULL,
    [Name] nvarchar(150)  NULL,
    [Recorder] nvarchar(150)  NOT NULL,
    [RecordDate] datetime  NOT NULL,
    [Status] int  NOT NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'tranTasks'
CREATE TABLE [dbo].[tranTasks] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [ProjectID] int  NOT NULL,
    [Name] nvarchar(150)  NOT NULL,
    [Remark] nvarchar(500)  NULL,
    [DueDate] datetime  NULL,
    [StartDate] datetime  NULL,
    [StaffID] int  NULL,
    [StatusID] int  NOT NULL,
    [PlatformID] int  NULL
);
GO

-- Creating table 'masPlatform'
CREATE TABLE [dbo].[masPlatform] (
    [ID] int  NOT NULL,
    [Name] nchar(150)  NULL,
    [Recorder] nchar(150)  NOT NULL,
    [RecordDate] datetime  NOT NULL,
    [Status] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'masProjects'
ALTER TABLE [dbo].[masProjects]
ADD CONSTRAINT [PK_masProjects]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'masStaffs'
ALTER TABLE [dbo].[masStaffs]
ADD CONSTRAINT [PK_masStaffs]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'masTaskStatus'
ALTER TABLE [dbo].[masTaskStatus]
ADD CONSTRAINT [PK_masTaskStatus]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [ID] in table 'tranTasks'
ALTER TABLE [dbo].[tranTasks]
ADD CONSTRAINT [PK_tranTasks]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'masPlatform'
ALTER TABLE [dbo].[masPlatform]
ADD CONSTRAINT [PK_masPlatform]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ProjectID] in table 'tranTasks'
ALTER TABLE [dbo].[tranTasks]
ADD CONSTRAINT [FK_tranTask_masProject]
    FOREIGN KEY ([ProjectID])
    REFERENCES [dbo].[masProjects]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tranTask_masProject'
CREATE INDEX [IX_FK_tranTask_masProject]
ON [dbo].[tranTasks]
    ([ProjectID]);
GO

-- Creating foreign key on [StaffID] in table 'tranTasks'
ALTER TABLE [dbo].[tranTasks]
ADD CONSTRAINT [FK_tranTask_masStaff]
    FOREIGN KEY ([StaffID])
    REFERENCES [dbo].[masStaffs]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tranTask_masStaff'
CREATE INDEX [IX_FK_tranTask_masStaff]
ON [dbo].[tranTasks]
    ([StaffID]);
GO

-- Creating foreign key on [StatusID] in table 'tranTasks'
ALTER TABLE [dbo].[tranTasks]
ADD CONSTRAINT [FK_tranTask_masTaskStatus]
    FOREIGN KEY ([StatusID])
    REFERENCES [dbo].[masTaskStatus]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tranTask_masTaskStatus'
CREATE INDEX [IX_FK_tranTask_masTaskStatus]
ON [dbo].[tranTasks]
    ([StatusID]);
GO

-- Creating foreign key on [PlatformID] in table 'tranTasks'
ALTER TABLE [dbo].[tranTasks]
ADD CONSTRAINT [FK_tranTasks_masPlatform]
    FOREIGN KEY ([PlatformID])
    REFERENCES [dbo].[masPlatform]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tranTasks_masPlatform'
CREATE INDEX [IX_FK_tranTasks_masPlatform]
ON [dbo].[tranTasks]
    ([PlatformID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------