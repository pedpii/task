﻿using System.Web;
using System.Web.Optimization;

namespace CodeRepository.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-1.10.2.js",
                        "~/Scripts/jquery-ui.unobtrusive-2.2.0.js"));
            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                    "~/Content/jquery-ui.css")); 
        }    
    }
}