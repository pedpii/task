﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public class Reporting
    {
        public   string ReportType { get; set; }
        public List<Parameter> Parameters { get; set; }
        public Reporting() {
            Parameters = new List<Parameter>();
        }
    }
    public class Parameter
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public Parameter() { }
        public Parameter(string Name, object Value) {
            this.Value = Value;
            this.Name = Name;
        }
    }
    public class ReporFilter
    {
        public string Date_Start { get; set; }
        public string Date_End { get; set; }
        public int Branch_ID { get; set; }
        public string LOGGING_LEVEL { get; set; }
        public string Branch_Name { get; set; }
        public string ReportCode { get; set; }
    }
    public class ReporProperty
    {
        public string ReportType { get; set; }
        public string ReportName { get; set; }
    }
}