﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Models
{
    public partial class tranTask
    {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        //public Nullable<System.DateTime> DueDate { get; set; }
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}")]
        //public Nullable<System.DateTime> StartDate { get; set; }
        public string DueDate { get; set; }
        public string StartDate { get; set; }
        public Nullable<int> StaffID { get; set; }
        public string StaffName { get; set; }
        public string Email { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }
        public string PlatformName { get; set; }
        public int PlatformID { get; set; }
        public int FileID { get; set; }
        public List<FileName> FileData { get; set; }
        public List<ListFileName> FileName { get; set; }
        public List<ListFileNameTs> FileNameTs { get; set; }

        public int imsg { get; set; }
        public int Approve { get; set; }
        public int StaffLevel { get; set; }
        //----------ชื่อผู้รับ ผู้ส่ง วันส่งทดสอบ----------//
        public int SenderID { get; set; }
        public string SenderName { get; set; }
        public int RecevierID { get; set; }
        public string RecevierIDName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}")]
        public System.DateTime DateReccode { get; set; }
        //--------------------------------------//
    }
    public class FileName
    {
        public string OldName { get; set; }
        public string Index { get; set; }
        public HttpPostedFileBase Files { get; set; }
        public string Comment { get; set; }
    }
    public class ListFileName
    {
        public string PathFile { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string GUID { get; set; }
        public HttpPostedFileBase Files { get; set; }
        public bool Review { get; set; }
        
    }
    public class ListFileNameTs
    {
        public string OldFiles { get; set; }
        public HttpPostedFileBase FilesData { get; set; }
        public string Comment { get; set; }
        public bool ReviewFiles { get; set; }
        public int IDmapTask { get; set; }
        public string PathFiles { get; set; }

    }
    public class Task_Filter
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public int StaffID { get; set; }
        public string StaffName { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }
        public string PlatformName { get; set; }
        public int PlatformID { get; set; }
        public int? RecorderID { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DueDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}")]
        public Nullable<System.DateTime> StartDate { get; set; }
        public DateTime? FilterDueDate { get; set; }
        public bool UnShowFinish { get; set; }
        public bool UnShowCancel { get; set; }
        public int Approve { get; set; }
        public string Comment { get; set; }
    }
    public class CreatetranTaskm
    {
        public int ID { get; set; }
        public int ProjectID { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
        public System.DateTime DueDate { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<int> StaffID { get; set; }
        public int StatusID { get; set; }
        public int PlatformID { get; set; }
        //รูปภาพ ไฟล์
        public int FileID { get; set; }
        //.รูปภาพ ไฟล์
    }
    public class EdittranTaskm
    {
        public int ID { get; set; }
        public int ProjectID { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
        public System.DateTime DueDate { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<int> StaffID { get; set; }
        public int StatusID { get; set; }
        public int PlatformID { get; set; }
        public int Approve { get; set; }
        //----------รูปภาพ ไฟล์  ----------//
        public int FileID { get; set; }
    }
    public partial class tranTasksh
    {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
        public string DueDate { get; set; }
        public string StartDate { get; set; }
        public string StaffID { get; set; }
        public string StaffName { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }
        public int PlatformID { get; set; }
        public string PlatformName { get; set; }
        public int Approve { get; set; }
        //----------รูปภาพ ไฟล์  ----------//
        public List<FileName> FileData { get; set; }
        public List<ListFileName> FileName { get; set; }
        public List<ListFileNameTs> FileNameTs { get; set; }
        public int FileID { get; set; }


        //----------ชื่อผู้รับ ผู้ส่ง วันส่งทดสอบ----------//
        public int SenderID { get; set; }
        public int RecevierID { get; set; }
        public System.DateTime DateReccode { get; set; }
        //--------------------------------------//
    }
    public class dropdownAuto
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}