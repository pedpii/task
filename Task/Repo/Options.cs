﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Repo
{
    static public class Options
    {
        public static bool EntityToModel<Entity, Model>(List<Entity> Sources, ref List<Model> Destinations) where Model : new()
        {
            foreach (var Source in Sources)
            {
                Model Destination = new Model();
                Destinations.Add(Destination);
                foreach (var prop in Destination.GetType().GetProperties())
                {
                    if (Source.GetType().GetProperty(prop.Name) != null)
                    {
                        prop.SetValue(Destination, Source.GetType().GetProperty(prop.Name).GetValue(Source));
                    }
                }
            }
            return true;
        }
    }
}