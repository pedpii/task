﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task.Class
{
    public  class Reporting
    {
        public static String ReportViewer(Models.Reporting Report) {
           string resume =" <div id=\"reportViewer1\">loading...</div>";
            resume+="<script>";
	        resume+="jQuery(function(){jQuery('#reportViewer1').telerik_ReportViewer({\"serviceUrl\":\"../api/ServiceReports/\"";
            resume += ",\"templateUrl\":\"../ReportViewer/templates/telerikReportViewerTemplate.html\",\"reportSource\":{\"report\":\"" + Report.ReportType+"\"";
            resume+=",\"parameters\":";
            if (Report.Parameters.Count <= 0) {
                resume += "{}"; 
            }
            else
            {
                resume += "{";
                int Count = 0;
                foreach (Models.Parameter p in Report.Parameters)
                {
                    if (Count == 0)
                    {
                        resume += "\"" + p.Name + "\":\"" + p.Value + "\"";
                    }
                    else
                    {
                        resume += ",\"" + p.Name + "\":\"" + p.Value + "\"";
                    }
                    Count++;
                }
                resume += "}";
            }
            resume+="}";
            resume+=",\"viewMode\":\"PRINT_PREVIEW\",\"scaleMode\":\"SPECIFIC\",\"scale\":1,\"persistSession\":false});});";
            resume+="</script>";
            return resume ;
        }
    }
}