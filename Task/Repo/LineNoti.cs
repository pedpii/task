﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Task;

namespace Repo
{
    public static class LineNoti
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg">ข้อความที่จะส่งใน Line</param>
        /// <param name="token">Token สำหรับ Line</param>
        /// <param name="lineApi">Link Api ของ Line</param>
        public static void lineNotify(string msg ,string token, string lineApi)
        {
            //string token = "3Wc44eB6Uff8uDciGOGl0K4hZXSau8P2s66IbS1hwlE";
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(lineApi);
                var postData = string.Format("message={0}", msg);
                var data = Encoding.UTF8.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                request.Headers.Add("Authorization", "Bearer " + token);

                using (var stream = request.GetRequestStream()) stream.Write(data, 0, data.Length);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}