﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Task;
namespace  Controllers
{
    public class HomeController : Controller
    {


        private  Repo.LogInData Ldata = new  Repo.LogInData();
        public ActionResult Index()
        {
              if (System.Web.HttpContext.Current.Session["Permission"] == null) 
              {
                return LogOff();//new HttpStatusCodeResult(HttpStatusCode.BadRequest, "NotAuthorized");
              } 
            if (System.Web.HttpContext.Current.Session["Culture"] == null)
            {
                System.Web.HttpContext.Current.Session["Culture"] = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
                System.Web.HttpContext.Current.Session["CultureName"] = System.Threading.Thread.CurrentThread.CurrentUICulture.EnglishName;
            }
            return View();
        }
        public ActionResult LogIn()
        {
            return View("LogIn", "~/Views/Shared/_LogIn.cshtml");
            //return View("Login");
        }
        [HttpPost]
        public ActionResult LogIn([Bind(Include = "UserLogin,PasswordLogin")] Models.Login Item)
        {
            JobsEntities db = new JobsEntities();
            
            var Staffs =db.masStaffs.Where(c => c.UserLogin == Item.UserLogin && c.PasswordLogin == Item.PasswordLogin);
            if (Staffs!= null && Staffs.Count() > 0)
            {
                HttpContext.Session["StaffLevel"] = Staffs.FirstOrDefault().StaffLevel; 
                HttpContext.Session["LoginID"] = Staffs.FirstOrDefault().UserLogin;
                HttpContext.Session["Name"] = Staffs.FirstOrDefault().Name;
                HttpContext.Session["IDStaff"]= Staffs.FirstOrDefault().ID;

                //return View("Project");
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "not valid User Or Password");
            }
            //return View("Login");
        }
        [HttpPost]
        public  ActionResult LogOff()
        {
                System.Web.HttpContext.Current.Session.Clear();
                return RedirectToAction("LogIn");// RedirectToAction("LogIn", "Home");
        }
        public ActionResult Edit(int ID)
        {
            if (ID == 1)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en");
                System.Web.HttpContext.Current.Session["Culture"] = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
                System.Web.HttpContext.Current.Session["CultureName"] = System.Threading.Thread.CurrentThread.CurrentUICulture.DisplayName ;
            }
            if (ID == 2)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("th");
                System.Web.HttpContext.Current.Session["Culture"] = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
                System.Web.HttpContext.Current.Session["CultureName"] = "ไทย";
            }
            return RedirectToAction("Index");
        }
	}
}