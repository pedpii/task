﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RestSharp;
using Models;
using System.Web.Script.Serialization;
using System.Net;
using System.Reflection;
using PagedList;

namespace Task.Controllers
{
    public class ProjectController : Controller
    {
        JobsEntities db = new JobsEntities();
        private int pageSize = 10;
        //
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List(Models.Project_Filter Filter, int pageNO = 1, int? RecorderID = null)
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            try
            {
                var Item = db.masProjects.Where(w => w.Status > 0).AsQueryable();
                if (Filter != null)
                {
                    if (Filter.ID > 0)
                    {
                        Item = Item.Where(w => w.ID == Filter.ID).AsQueryable();
                    }
                    if (!string.IsNullOrWhiteSpace(Filter.Name))
                    {
                        Item = Item.Where(w => w.Name.Contains(Filter.Name)).AsQueryable();
                    }
                }
                int.TryParse(Properties.Settings.Default.pageSize.ToString(), out pageSize);
                if (pageSize <= 0) { pageSize = 10; }
                int pageIdx = pageNO - 1;
                int totalItemCount = Item.Count();
                var paged = Item.OrderBy(o => o.Name).Skip(pageIdx * pageSize).Take(pageSize);
                var pageresult = new StaticPagedList<masProjects>(paged, pageNO, pageSize, totalItemCount);
                ViewBag.RecorderID = RecorderID;
                return PartialView(pageresult);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
        //private Models.Project_Filter EntityToModel(tranTask Value)
        //{
        //    return new Models.Project_Filter()
        //    {
        //        ID = Value.ID,
        //        Name = Value.Name,
        //        DueDate = Value.DueDate,
        //        ProjectID = Value.ProjectID,
        //        ProjectName= Value.masProject.Name,
        //        Remark = Value.Remark,
        //        StaffID = Value.StaffID,
        //        StaffName = Value.masStaff==null? "":Value.masStaff.Name,
        //        StartDate = Value.StartDate,
        //        StatusID = Value.StatusID,
        //        StatusName = Value.masTaskStatu.Name
        //    };
        //}
        public ActionResult Create()
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            Project_Filter Item = new Project_Filter();
            return PartialView(Item);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Models.Project_Filter Item)
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            string ErrorMSG = ValidForNew(Item, true); //ฟังชั่นเช็คค่าว่างกับตัวอักษร true กับ false
            if (!ErrorMSG.Equals(string.Empty))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ErrorMSG);
            }
            if (Item != null)
            {
                var getid = db.masProjects;
                masProjects data = new masProjects()
                {

                    Name = Item.Name,
                    Recorder = StaffLevel.ToString(),
                    RecordDate = DateTime.Now,
                    Status = 1,
                };
                db.masProjects.Add(data);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public ActionResult Edit(int ID)
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            Models.Project_Filter Item = new Project_Filter();
            var getmassProjects = db.masProjects.FirstOrDefault(s => s.ID == ID);
            if (getmassProjects != null)
            {
                Item.ID = getmassProjects.ID;
                Item.Name = getmassProjects.Name;
                Item.Recorder = StaffLevel.ToString();
                Item.RecordDate = DateTime.Now;
                Item.Status = 1;
            }

            return PartialView(Item);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Models.Project_Filter Item)
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            string ErrorMSG = ValidForNew(Item, true); //ฟังชั่นเช็คค่าว่างกับตัวอักษร true กับ false
            if (!ErrorMSG.Equals(string.Empty))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ErrorMSG);
            }
            if (Item != null)
            {
                var getidmasProjects = db.masProjects.FirstOrDefault(s => s.ID == Item.ID);
                if (getidmasProjects != null)
                {
                    getidmasProjects.Name = Item.Name;
                    getidmasProjects.Recorder = StaffLevel.ToString();
                    getidmasProjects.RecordDate = DateTime.Now;
                    getidmasProjects.Status = 1;
                }
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        public string ValidForNew(Models.Project_Filter Item, bool create = true) //create=true ให้เช็คค่าเลยกำหนดใน create กับ Edit ที่สร้างมา
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            string resume = string.Empty;
            string ErrorAlert = string.Empty;
            string ErrorRow = string.Empty;
            //int n;

            if (Item != null)
            {
                if (create)
                {
                    if (Item.Name == null)
                    {
                        resume += "AA,";
                    }
                    else if (Item.Name.Count() >= 255)
                    {
                        resume += "AB,";
                    }

                }
                else
                {
                    if (Item.Name == null)
                    {
                        resume += "AA,";
                    }
                    else if (Item.Name.Count() >= 255)
                    {
                        resume += "AB,";
                    }
                }

                ErrorRow += ";";
            }

            resume += "/" + ErrorRow;
            resume += "/" + ErrorAlert;
            string resumetest = resume.Replace('/', ' ');
            resumetest = resumetest.Replace(';', ' ');
            if (resumetest.Trim() == string.Empty) { resume = string.Empty; }
            return resume;
        }
        public ActionResult Delete(int ID)
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];
            var Item = db.masProjects.Find(ID);
            return PartialView(Item);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Models.Project_Filter Item)
        {
            int StaffLevel = HttpContext.Session["StaffLevel"] == null ? 0 : (int)HttpContext.Session["StaffLevel"];

            if (Item != null)
            {
                var getidmasProjects = db.masProjects.FirstOrDefault(s => s.ID == Item.ID);
                if (getidmasProjects != null)
                {
                    getidmasProjects.Recorder = StaffLevel.ToString();
                    getidmasProjects.RecordDate = DateTime.Now;
                    getidmasProjects.Status = 0;
                }
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        //    try
        //    {
        //        if (HttpContext.Session["Emp_ID"] == null)
        //        {
        //            return RedirectToAction("LogIn", "Home");
        //        }
        //        ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, SslPolicyErrors) => true);
        //        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

        //        var clientsta = new RestClient(webAPIURL + "/api/pf_divisionApi?search={BRANCH_ID:'" + Branch_ID + "', DIVISION_ID:'" + Division_ID + "', PROFILE_ID:"+Profile_ID+"}&Full=true");
        //        var requeststa = new RestRequest(Method.GET);
        //        requeststa.AddHeader("postman-token", "0f6bdaf6-04ac-2bd1-28f1-bafb5ac05519");
        //        requeststa.AddHeader("cache-control", "no-cache");
        //        IRestResponse responsesta = clientsta.Execute(requeststa);
        //        List<Models.AlarmDivision> myAlarmDiv= new List<AlarmDivision>();
        //        if (responsesta.StatusCode.ToString() == "OK")
        //        {
        //            myAlarmDiv = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.AlarmDivision>>(responsesta.Content);
        //        }
        //        Models.AlarmDivision item = new Models.AlarmDivision();
        //        item = myAlarmDiv.FirstOrDefault();
        //        if (item == null)
        //        {
        //            item = new AlarmDivision();
        //            item.BRANCH_ID = int.Parse(Branch_ID);
        //            item.PROFILE_ID = int.Parse(Profile_ID);
        //            item.DIVISION_ID = int.Parse(Division_ID);
        //            item.DIVISION_Name = Division_Name;
        //        }
        //        return PartialView(item);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.ToString());
        //    }
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(AlarmDivision Item)
        //{
        //    string empID;
        //    if (HttpContext.Session["Emp_ID"] == null)
        //    {
        //        return RedirectToAction("LogIn", "Home");
        //    }
        //    else
        //    {
        //        empID = HttpContext.Session["Emp_ID"].ToString();
        //    }
        //    try
        //    {
        //        AlarmDivisionPF ItemPF = new AlarmDivisionPF();
        //        ItemPF.BRANCH_ID = Item.BRANCH_ID;
        //        ItemPF.PROFILE_ID = Item.PROFILE_ID;
        //        ItemPF.DIVISION_ID = Item.DIVISION_ID;
        //        ItemPF.QALPHABET = Item.QALPHABET;
        //        ItemPF.QTYPE = Item.QTYPE;
        //        ItemPF.QSTART = Item.QSTART;
        //        ItemPF.QEND = Item.QEND;
        //        ItemPF.PRINT_COPIES = Item.PRINT_COPIES;
        //        ItemPF.SHOW = Item.SHOW;
        //        ItemPF.SEQ = Item.SEQ;
        //        ItemPF.ISPRIORITYQ = Item.ISPRIORITYQ;
        //        ItemPF.PRIORITYQ_TYPE = Item.PRIORITYQ_TYPE;
        //        ItemPF.PRIORITYQ_TIMESEC = Item.PRIORITYQ_TIMESEC;
        //        ItemPF.PRIORITYQ_Q = Item.PRIORITYQ_Q;
        //        ItemPF.WAITQ_TYPE = Item.WAITQ_TYPE;
        //        ItemPF.WAITINGTIME_WARN = Item.WAITINGTIME_WARN;
        //        ItemPF.WAITINGTIME_ALERT = Item.WAITINGTIME_ALERT;
        //        ItemPF.WAITINGQUEUE_WARN = Item.WAITINGQUEUE_WARN;
        //        ItemPF.WAITINGQUEUE_ALERT = Item.WAITINGQUEUE_ALERT;
        //        ItemPF.SERVSEC_WARN = Item.SERVSEC_WARN;
        //        ItemPF.SERVSEC_ALERT = Item.SERVSEC_ALERT;
        //        string ErrorMSG = ValidForNew(Item);
        //        if (!ErrorMSG.Equals(string.Empty)) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ErrorMSG); }
        //        string LoginUserName = HttpContext.Session["NameStaff"]==null?"":HttpContext.Session["NameStaff"].ToString();
        //        string dataJson = Newtonsoft.Json.JsonConvert.SerializeObject(ItemPF);
        //        var clientAdd = new RestClient(webAPIURL + "/api/pf_divisionApi");
        //        var requestAdd = new RestRequest(Method.POST);
        //        requestAdd.AddHeader("postman-token", "73930bc9-07a6-5055-4a86-9c2874e32a2f");
        //        requestAdd.AddHeader("cache-control", "no-cache");
        //        requestAdd.AddHeader("content-type", "application/json");
        //        //requestAdd.AddHeader("LoginUserName",LoginUserName);
        //        requestAdd.AddHeader("LoginUserName", empID);
        //        requestAdd.AddParameter("application/json", dataJson, ParameterType.RequestBody);
        //        IRestResponse responseAdd = clientAdd.Execute(requestAdd);
        //        if (responseAdd.StatusCode.ToString() != "OK")
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "//ZZ");
        //        }

        //    }
        //    catch
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "//ZZ");
        //    }
        //    return PartialView(Item);
        //}
        //[HttpPost]
        //public ActionResult SaveMultiBranch(int branch, string branchAll)
        //{
        //    string status = "";
        //    string empID;
        //    if (HttpContext.Session["Emp_ID"] == null)
        //    {
        //        return RedirectToAction("LogIn", "Home");
        //    }
        //    else
        //    {
        //        empID = HttpContext.Session["Emp_ID"].ToString();
        //    }
        //    try
        //    {

        //        ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, SslPolicyErrors) => true);
        //        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

        //        //Check GroupInfo Before Save As
        //        string BranchDiv = "";
        //        var clientdiv = new RestClient(webAPIURL + "/api/divinfoapi?search={Branch_ID:'" + branch + "'}");
        //        var requestdiv = new RestRequest(Method.GET);
        //        requestdiv.AddHeader("postman-token", "0f6bdaf6-04ac-2bd1-28f1-bafb5ac05519");
        //        requestdiv.AddHeader("cache-control", "no-cache");
        //        IRestResponse responsediv = clientdiv.Execute(requestdiv);
        //        List<Task.tranTask> datadiv = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Task.tranTask>>(responsediv.Content);

        //        string[] ArrBranchAll = branchAll.Split(',');
        //        for (int i = 0; i < ArrBranchAll.Count(); i++)
        //        {
        //            var clientdivcheck = new RestClient(webAPIURL + "/api/divinfoapi?search={Branch_ID:'" + ArrBranchAll[i] + "'}");
        //            var requestdivcheck = new RestRequest(Method.GET);
        //            requestdivcheck.AddHeader("postman-token", "0f6bdaf6-04ac-2bd1-28f1-bafb5ac05519");
        //            requestdivcheck.AddHeader("cache-control", "no-cache");
        //            IRestResponse responsedivcheck = clientdivcheck.Execute(requestdivcheck);
        //            List<Task.tranTask> datadivcheck = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Task.tranTask>>(responsedivcheck.Content);
        //            if (datadivcheck.Count() >= datadiv.Count())
        //            {
        //                int count = datadivcheck.Count(W => datadiv.Select(S => S.ID).Contains(W.ID));
        //                if (count < datadiv.Count())
        //                {
        //                    BranchDiv += ArrBranchAll[i] + ",";
        //                }
        //            }
        //            else
        //            {
        //                BranchDiv += ArrBranchAll[i] + ",";
        //            }
        //        }
        //        if (BranchDiv.Length > 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "A/" + BranchDiv.Remove(BranchDiv.Length - 1)); }


        //        var clientAlarm = new RestClient(webAPIURL + "/api/pf_divisionApi?search={BRANCH_ID:'" + branch +"', PROFILE_ID:'"+1+"'}");
        //        var requestAlarm = new RestRequest(Method.GET);
        //        requestAlarm.AddHeader("postman-token", "0f6bdaf6-04ac-2bd1-28f1-bafb5ac05519");
        //        requestAlarm.AddHeader("cache-control", "no-cache");
        //        IRestResponse responseAlarm = clientAlarm.Execute(requestAlarm);
        //        List<Models.AlarmDivisionPF> data = new List<AlarmDivisionPF>();
        //        if (responseAlarm.StatusCode.ToString() == "OK")
        //        {
        //            data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.AlarmDivisionPF>>(responseAlarm.Content);
        //        }
        //        else
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "C/  :" + responseAlarm.StatusCode);
        //        }
        //        if (data.Count == 0 || data == null)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "D/");
        //        }
        //        for (int i = 0; i < ArrBranchAll.Count(); i++)
        //        {
        //            for (int j = 0; j < data.Count(); j++)
        //            {
        //                data[j].BRANCH_ID = int.Parse(ArrBranchAll[i]);
        //            }
        //            string dataJ = Newtonsoft.Json.JsonConvert.SerializeObject(data);
        //            var client = new RestClient(webAPIURL + "/api/AlarmDivisionApi");
        //            ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
        //            var request = new RestRequest(Method.POST);
        //            request.AddHeader("postman-token", "3b3a3d88-d99e-c70e-d28a-d26c0c784036");
        //            request.AddHeader("cache-control", "no-cache");
        //            request.AddHeader("content-type", "application/json");
        //            request.AddHeader("LoginUserName", empID);
        //            request.AddParameter("application/json", dataJ, ParameterType.RequestBody);
        //            IRestResponse response = client.Execute(request);
        //            if (response.StatusCode.ToString() != "OK")
        //            {
        //                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "C/ :" + response.StatusCode);
        //            }
        //        }
        //        if (status.Length > 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest, status); }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "C/ :" + ex.ToString());
        //    }
        //    return new HttpStatusCodeResult(HttpStatusCode.OK);
        //}
        //public string ValidForNew(AlarmDivision Item)
        //{
        //    string resume = string.Empty;
        //    string ErrorAlert = string.Empty;
        //    string ErrorRow = string.Empty;
        //    int n;
        //    if (Item != null)
        //    {
        //        //if (string.IsNullOrWhiteSpace(Item.WAITINGQUEUE_WARN.ToString()))
        //        //{
        //        //    resume += "AA,";
        //        //}
        //        //else
        //        //{
        //        //    if (!int.TryParse(Item.WAITINGQUEUE_WARN.ToString(),out n))
        //        //    {
        //        //        resume += "AB,";
        //        //    }
        //        //    if (n < 0)
        //        //    {
        //        //        resume += "AC,";
        //        //    }
        //        //    if (n > 100000)
        //        //    {
        //        //        resume += "AA,";
        //        //    }
        //        //}
        //        if (string.IsNullOrWhiteSpace(Item.WAITINGQUEUE_ALERT.ToString()))
        //        {
        //            resume += "BA,";
        //        }
        //        else
        //        {
        //            if (!int.TryParse(Item.WAITINGQUEUE_ALERT.ToString(), out n))
        //            {
        //                resume += "BB,";
        //            }
        //            if (n < 0)
        //            {
        //                resume += "BC,";
        //            }
        //            if (n > 100000)
        //            {
        //                resume += "BA,";
        //            }
        //        }
        //        //if (string.IsNullOrWhiteSpace(Item.WAITINGTIME_WARN.ToString()))
        //        //{
        //        //    resume += "CA,";
        //        //}
        //        //else
        //        //{
        //        //    if (!int.TryParse(Item.WAITINGTIME_WARN.ToString(), out n))
        //        //    {
        //        //        resume += "CB,";
        //        //    }
        //        //    if (n < 0)
        //        //    {
        //        //        resume += "CC,";
        //        //    }
        //        //    if (n > 100000)
        //        //    {
        //        //        resume += "CA,";
        //        //    }
        //        //}
        //        if (string.IsNullOrWhiteSpace(Item.WAITINGTIME_ALERT.ToString()))
        //        {
        //            resume += "DA,";
        //        }
        //        else
        //        {
        //            if (!int.TryParse(Item.WAITINGTIME_ALERT.ToString(), out n))
        //            {
        //                resume += "DB,";
        //            }
        //            if (n < 0)
        //            {
        //                resume += "DC,";
        //            }
        //            if (n > 100000)
        //            {
        //                resume += "DA,";
        //            }
        //        }
        //        //if (string.IsNullOrWhiteSpace(Item.SERVSEC_WARN.ToString()))
        //        //{
        //        //    resume += "EA,";
        //        //}
        //        //else
        //        //{
        //        //    if (!int.TryParse(Item.SERVSEC_WARN.ToString(), out n))
        //        //    {
        //        //        resume += "EB,";
        //        //    }
        //        //    if (n < 0)
        //        //    {
        //        //        resume += "EC,";
        //        //    }
        //        //    if (n > 100000)
        //        //    {
        //        //        resume += "EA,";
        //        //    }
        //        //}
        //        if (string.IsNullOrWhiteSpace(Item.SERVSEC_ALERT.ToString()))
        //        {
        //            resume += "FA,";
        //        }
        //        else
        //        {
        //            if (!int.TryParse(Item.SERVSEC_ALERT.ToString(), out n))
        //            {
        //                resume += "FB,";
        //            }
        //            if (n < 0)
        //            {
        //                resume += "FC,";
        //            }
        //            if (n > 100000)
        //            {
        //                resume += "FA,";
        //            }
        //        }

        //        ErrorRow += ";";
        //    }

        //    resume += "/" + ErrorRow;
        //    resume += "/" + ErrorAlert;
        //    string resumetest = resume.Replace('/', ' ');
        //    resumetest = resumetest.Replace(';', ' ');
        //    if (resumetest.Trim() == string.Empty) { resume = string.Empty; }
        //    return resume;
        //}
    }
}
