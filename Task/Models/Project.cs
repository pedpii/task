﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public class Project_Filter 
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Recorder { get; set; }
        public System.DateTime RecordDate { get; set; }
        public int Status { get; set; }
    }
}