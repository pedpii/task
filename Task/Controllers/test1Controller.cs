﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace Task.Controllers
{
    public class test1Controller : Controller
    {
        //
        // GET: /test1/
        public ActionResult Index()
        {
            Models.testDisplay1 t = new Models.testDisplay1();
            if (System.Web.HttpContext.Current.Cache.Get("Cache1") != null)
            {
                t.Cache1 = System.Web.HttpRuntime.Cache.Get("Cache1").ToString();

            }
            if (System.Web.HttpContext.Current.Session["Session1"] != null)
            {
                t.Session1 = System.Web.HttpContext.Current.Session["Session1"].ToString();
            }
            if (System.Web.HttpContext.Current.Application["Application1"] != null)
            {
                t.Application1 = System.Web.HttpContext.Current.Application["Application1"].ToString();
            }
            return View(t);
        }

        //
        // GET: /test1/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /test1/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /test1/Create
        [HttpPost]
        public ActionResult Create(Models.test1 Item)
        {
            try
            {
                // TODO: Add insert logic here
                System.Web.HttpContext.Current.Session["Session1"] = Item.Name;
                System.Web.HttpContext.Current.Application["Application1"] = Item.Name;
                DateTime aa= DateTime.Now.AddMinutes(10);
//System.Web.Caching.OutputCacheProvider = 
                System.Web.HttpRuntime.Cache.Insert("Cache1", Item.Name, null, aa, TimeSpan.Zero, CacheItemPriority.High, null); 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /test1/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /test1/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /test1/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /test1/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
