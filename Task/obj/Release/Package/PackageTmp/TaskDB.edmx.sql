
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 07/01/2019 14:59:50
-- Generated from EDMX file: C:\INNET\RDManagement\Task\Task\TaskDB.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Jobs];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_tranTask_masProject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tranTask] DROP CONSTRAINT [FK_tranTask_masProject];
GO
IF OBJECT_ID(N'[dbo].[FK_tranTask_masStaff]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tranTask] DROP CONSTRAINT [FK_tranTask_masStaff];
GO
IF OBJECT_ID(N'[dbo].[FK_tranTask_masTaskStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tranTask] DROP CONSTRAINT [FK_tranTask_masTaskStatus];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[masProject]', 'U') IS NOT NULL
    DROP TABLE [dbo].[masProject];
GO
IF OBJECT_ID(N'[dbo].[masStaff]', 'U') IS NOT NULL
    DROP TABLE [dbo].[masStaff];
GO
IF OBJECT_ID(N'[dbo].[masTaskStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[masTaskStatus];
GO
IF OBJECT_ID(N'[dbo].[tranTask]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tranTask];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'masProjects'
CREATE TABLE [dbo].[masProjects] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(150)  NOT NULL,
    [Recorder] nvarchar(150)  NOT NULL,
    [RecordDate] datetime  NOT NULL,
    [Status] int  NOT NULL
);
GO

-- Creating table 'masTaskStatus'
CREATE TABLE [dbo].[masTaskStatus] (
    [ID] int  NOT NULL,
    [Name] nvarchar(150)  NULL,
    [Recorder] nvarchar(150)  NOT NULL,
    [RecordDate] datetime  NOT NULL,
    [Status] int  NOT NULL
);
GO

-- Creating table 'tranTasks'
CREATE TABLE [dbo].[tranTasks] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [ProjectID] int  NOT NULL,
    [Name] nvarchar(150)  NOT NULL,
    [Remark] nvarchar(500)  NULL,
    [DueDate] datetime  NULL,
    [StartDate] datetime  NULL,
    [StaffID] int  NULL,
    [StatusID] int  NOT NULL
);
GO

-- Creating table 'masStaffs'
CREATE TABLE [dbo].[masStaffs] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(150)  NOT NULL,
    [Recorder] nvarchar(150)  NOT NULL,
    [RecordDate] datetime  NOT NULL,
    [UserLogin] varchar(50)  NULL,
    [PasswordLogin] varchar(50)  NULL,
    [StaffLevel] int  NOT NULL,
    [Status] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'masProjects'
ALTER TABLE [dbo].[masProjects]
ADD CONSTRAINT [PK_masProjects]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'masTaskStatus'
ALTER TABLE [dbo].[masTaskStatus]
ADD CONSTRAINT [PK_masTaskStatus]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'tranTasks'
ALTER TABLE [dbo].[tranTasks]
ADD CONSTRAINT [PK_tranTasks]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'masStaffs'
ALTER TABLE [dbo].[masStaffs]
ADD CONSTRAINT [PK_masStaffs]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ProjectID] in table 'tranTasks'
ALTER TABLE [dbo].[tranTasks]
ADD CONSTRAINT [FK_tranTask_masProject]
    FOREIGN KEY ([ProjectID])
    REFERENCES [dbo].[masProjects]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_tranTask_masProject'
CREATE INDEX [IX_FK_tranTask_masProject]
ON [dbo].[tranTasks]
    ([ProjectID]);
GO

-- Creating foreign key on [StatusID] in table 'tranTasks'
ALTER TABLE [dbo].[tranTasks]
ADD CONSTRAINT [FK_tranTask_masTaskStatus]
    FOREIGN KEY ([StatusID])
    REFERENCES [dbo].[masTaskStatus]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_tranTask_masTaskStatus'
CREATE INDEX [IX_FK_tranTask_masTaskStatus]
ON [dbo].[tranTasks]
    ([StatusID]);
GO

-- Creating foreign key on [StaffID] in table 'tranTasks'
ALTER TABLE [dbo].[tranTasks]
ADD CONSTRAINT [FK_tranTask_masStaff]
    FOREIGN KEY ([StaffID])
    REFERENCES [dbo].[masStaffs]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_tranTask_masStaff'
CREATE INDEX [IX_FK_tranTask_masStaff]
ON [dbo].[tranTasks]
    ([StaffID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------