﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using Task;

namespace Repo
{
    public static class sendEmail
    {
        public static bool SendMailToUser(Models.SendEmail Item , int ID)
        {
            bool result = false;
            using (JobsEntities db = new JobsEntities())
            {
                string emailsend = "";
                var rsEmail = db.masStaffs.Where(w => w.Status > 0 && w.ID == Item.StaffID).FirstOrDefault() ;
                if (rsEmail != null)
                {
                    emailsend = rsEmail.Email ?? "";
                }
                string mmsg = "";
                if (Item.imsg == 1)
                {
                    mmsg = "Create Task ";
                }
                else if (Item.imsg == 2)
                {
                    mmsg = "Edit Task ";
                }

                var msg = "";
                if (Item.StatusID == 4)
                {
                    string urlte = "http://10.172.101.131/TsTask/" + "TsEdits?TaskId=" + ID.ToString();
                    msg = mmsg + "<br />ProjectID : " + Item.ProjectName + "<br />Task : " + Item.Name + "<br />วันที่เริ่มงาน : "
                    + Item.StartDate + "<br />กำหนดส่ง : " + Item.DueDate + "<br />Remark : " + Item.Remark + "<br />ผู้รับผิดชอบ : "
                    + Item.StaffName + "<br />สถานะ : " + Item.StatusName + "<br />รูปแบบ : " + Item.PlatformName
                    + "<br />Link : " + "<a href=" + urlte + " target=" + "_blank" + ">Link</a>\n";
                }
                else
                {
                    string urlte = Task.Properties.Settings.Default.LinkURL + "/Task/Details?TaskId=" + ID.ToString();
                    msg = mmsg + "<br />ProjectID : " + Item.ProjectName + "<br />Task : " + Item.Name + "<br />วันที่เริ่มงาน : "
                    + Item.StartDate + "<br />กำหนดส่ง : " + Item.DueDate + "<br />Remark : " + Item.Remark + "<br />ผู้รับผิดชอบ : "
                    + Item.StaffName + "<br />สถานะ : " + Item.StatusName + "<br />รูปแบบ : " + Item.PlatformName
                    + "<br />Link : " + "<a href=" + urlte + " target=" + "_blank" + ">Link</a>\n";
                }
                

                result = Repo.sendEmail.SendEmail(emailsend, mmsg, msg);
                return result;
            }

        }
        public static bool SendEmail(string toEmail, string subject, string emailBody)
        {
            try
            {
                string email = Task.Properties.Settings.Default.Email;
                string password = Task.Properties.Settings.Default.Password;

                SmtpClient client = new SmtpClient();
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.EnableSsl = true;
                //client.Timeout = 100000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;

                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(email, password);
                client.Credentials = credentials;

                MailMessage mailMessage = new MailMessage(email, toEmail, subject, emailBody);
                mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = UTF8Encoding.UTF8;
                client.Send(mailMessage);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}