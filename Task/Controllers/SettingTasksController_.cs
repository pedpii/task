﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Task;

namespace Task.Controllers
{
    public class SettingTasksController_ : Controller
    {
        private JobsEntities db = new JobsEntities();
        private int pageSize = 10;
        // GET: /SettingTasks/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List(Models.Task_Filter Filter, int pageNO = 1)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, SslPolicyErrors) => true);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                var Task = db.tranTasks.AsQueryable();
                if (Filter != null)
                {
                    if (Filter.ProjectID > 0)
                    {
                        Task = Task.Where(w => w.ProjectID == Filter.ProjectID).AsQueryable();
                    }
                    if (Filter.StaffID > 0)
                    {
                        Task = Task.Where(w => w.StaffID == Filter.StaffID).AsQueryable();
                    }
                    if (Filter.StatusID >= 0)
                    {
                        Task = Task.Where(w => w.StatusID == Filter.StatusID).AsQueryable();
                    }
                }
                List<Models.tranTask> MTask = new List<Models.tranTask>();
                MTask = Task.Select(EntityToModel).ToList();
                int.TryParse(Properties.Settings.Default.pageSize.ToString(), out pageSize);
                if (pageSize <= 0) { pageSize = 10; }
                int pageIdx = pageNO - 1;
                int totalItemCount = MTask.Count();
                var paged = MTask.OrderBy(o => o.DueDate).Skip(pageIdx * pageSize).Take(pageSize);
                var pageresult = new StaticPagedList<Models.tranTask>(paged, pageNO, pageSize, totalItemCount);

                return PartialView(pageresult);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
        // GET: /SettingTasks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tranTask trantask = db.tranTasks.Find(id);
            if (trantask == null)
            {
                return HttpNotFound();
            }
            return View(trantask);
        }

        // GET: /SettingTasks/Create
        public ActionResult Create()
        {
            ViewBag.ProjectID = new SelectList(db.masProjects, "ID", "Name");
            ViewBag.StaffID = new SelectList(db.masStaffs, "ID", "Name");
            ViewBag.StatusID = new SelectList(db.masTaskStatus, "ID", "Name");
            return View();
        }

        // POST: /SettingTasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,ProjectID,Name,Remark,DueDate,StartDate,StaffID,StatusID")] tranTask trantask)
        {
            if (ModelState.IsValid)
            {
                db.tranTasks.Add(trantask);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProjectID = new SelectList(db.masProjects, "ID", "Name", trantask.ProjectID);
            ViewBag.StaffID = new SelectList(db.masStaffs, "ID", "Name", trantask.StaffID);
            ViewBag.StatusID = new SelectList(db.masTaskStatus, "ID", "Name", trantask.StatusID);
            return View(trantask);
        }

        // GET: /SettingTasks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tranTask trantask = db.tranTasks.Find(id);
            if (trantask == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProjectID = new SelectList(db.masProjects, "ID", "Name", trantask.ProjectID);
            ViewBag.StaffID = new SelectList(db.masStaffs, "ID", "Name", trantask.StaffID);
            ViewBag.StatusID = new SelectList(db.masTaskStatus, "ID", "Name", trantask.StatusID);
            return View(trantask);
        }

        // POST: /SettingTasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,ProjectID,Name,Remark,DueDate,StartDate,StaffID,StatusID")] tranTask trantask)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trantask).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProjectID = new SelectList(db.masProjects, "ID", "Name", trantask.ProjectID);
            ViewBag.StaffID = new SelectList(db.masStaffs, "ID", "Name", trantask.StaffID);
            ViewBag.StatusID = new SelectList(db.masTaskStatus, "ID", "Name", trantask.StatusID);
            return View(trantask);
        }

        // GET: /SettingTasks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tranTask trantask = db.tranTasks.Find(id);
            if (trantask == null)
            {
                return HttpNotFound();
            }
            return View(trantask);
        }

        // POST: /SettingTasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tranTask trantask = db.tranTasks.Find(id);
            db.tranTasks.Remove(trantask);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
