﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Task
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class JobsEntities : DbContext
    {
        public JobsEntities()
            : base("name=JobsEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<mapTaskTsDetail> mapTaskTsDetail { get; set; }
        public virtual DbSet<masPlatform> masPlatform { get; set; }
        public virtual DbSet<masProjects> masProjects { get; set; }
        public virtual DbSet<masStaffs> masStaffs { get; set; }
        public virtual DbSet<masTaskStatus> masTaskStatus { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<tranHistory> tranHistory { get; set; }
        public virtual DbSet<tranTasks> tranTasks { get; set; }
    }
}
