﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public class Staff_Filter 
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Recorder { get; set; }
        public System.DateTime RecordDate { get; set; }
        public string UserLogin { get; set; }
        public string PasswordLogin { get; set; }
        public int StaffLevel { get; set; }
        public int Status { get; set; }
        public string Email { get; set; }
    }
    public class StaffLV
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class EditStaffm
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Recorder { get; set; }
        public System.DateTime RecordDate { get; set; }
        public string UserLogin { get; set; }
        public string PasswordLogin { get; set; }
        public int StaffLevel { get; set; }
        public int Status { get; set; }
    }
}